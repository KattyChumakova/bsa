function calc(x, y) {
  let result = 0;
  
  if (x < y) {result = x + y}
  else if (x > y) {result = x - y}
	else if (x == y) {result = x * y};
  
  return result;
}


function SummAllArrayItems(a, b){
  let result = 0;
  let sum1 = 0;
  let sum2 = 0;

  for(let i = 0; i <= a.length; i++){
    sum1 = sum1 + i;
  }
  for(let i = 0; i <= b.length; i++){
    sum2 = sum2 + i;
  }
  
  result = sum1 + sum2;

  return result;
}

function SummTrueItems(a){
  let result = 0;
  
  for(let i = 0; i <= a.length; i++){
    if (a[i] === true)
        result++;
  }

 return result;  
}

function doublFactorial(n){
  let result = 0;
 
  if (n < 1) result = 1;
  else  {
    result = (n * doublFactorial(n - 2))
  }
  
  return result;
}

function compareAge(a, b){
  let result = "";
  
  if (a.age < b.age) result = a.name + " is younger " + b.name;
  if (a.age > b.age) result = a.name + " is older " + b.name;
  if (a.age == b.age) result = a.name + " the same age as " + b.name;
  
  return result;
}

